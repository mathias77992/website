misureeth = 'ETH'


$("#tile-1 .nav-tabs a").click(function () {
  var position = $(this).parent().position();
  var width = $(this).parent().width();
  $("#tile-1 .slider").css({ "left": + position.left, "width": width });
});
var actWidth = $("#tile-1 .nav-tabs").find(".active").parent("li").width();
var actPosition = $("#tile-1 .nav-tabs .active").position();


$(document).ready(function () {
  $('tbody').empty();

  $.getJSON("https://eth.protonmine.com/api/blocks", function (result) {
    $.getJSON("https://eth.protonmine.com/api/stats", function (data) {
      $.getJSON('https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD', function (dataz) {

        $('#total').text(result.maturedTotal);
        $('#immature').text(result.immatureTotal);
        $('#candidate').text(result.candidatesTotal);
        $.each(result.matured, function (i, field) {

          var dateTimeString = moment.unix(field.timestamp).format("DD-MM-YYYY HH:mm:ss");
          var amount = field.reward / 1000000000000000000;
          amount = amount.toFixed(3);
          var misure = misureeth;
          var usd = amount * dataz.USD;
          usd = usd.toFixed(1);
          var misureu = '$';
          var variance = field.shares / data.nodes[0].difficulty * 100;
          variance = variance.toFixed(1);
          var misurev = '%';
          $("#data").append('<tr><td><a rel="nofollow" target="_blank" href="https://blockscout.com/eth/mainnet/blocks/' + field.height + '">' + field.height + '</a></td>' + '<td>' + amount + ' ' + misure + ' ' + '/' + ' ' + usd + misureu + '</td>' + '<td>' + field.hash + '</td>' + '<td>' + dateTimeString + '</td>' + '<td>' + variance + '' + misurev + '</td></tr>');

        });
        $.each(result.immature, function (i, lul) {

          var dateTimeString = moment.unix(lul.timestamp).format("DD-MM-YYYY HH:mm:ss");
          var amount = lul.reward / 1000000000000000000;
          amount = amount.toFixed(3);
          var misure = 'ETH';
          var usd = amount * dataz.USD;
          usd = usd.toFixed(1);
          var misureu = '$';
          var variance = lul.shares / data.nodes[0].difficulty * 100;
          variance = variance.toFixed(1);
          var misurev = '%';
          $("#immature").append('<tr><td><a rel="nofollow" target="_blank" href="https://blockscout.com/eth/mainnet/blocks/' + lul.height + '">' + lul.height + '</a></td>' + '<td>' + amount + ' ' + misure + ' ' + '/' + ' ' + usd + misureu + '</td>' + '<td>' + lul.hash + '</td>' + '<td>' + dateTimeString + '</td>' + '<td>' + variance + '' + misurev + '</td></tr>');

        });
        $.each(result.candidates, function (i, field) {

          var dateTimeString = moment.unix(field.timestamp).format("DD-MM-YYYY HH:mm:ss");
          var amount = field.reward / 1000000000000000000;
          amount = amount.toFixed(3);
          var misure = 'ETH';
          var usd = amount * dataz.USD;
          usd = usd.toFixed(1);
          var misureu = '$';
          var variance = field.shares / data.nodes[0].difficulty * 100;
          variance = variance.toFixed(1);
          var misurev = '%';
          $("#new").append('<tr><td><a rel="nofollow" target="_blank" href="https://blockscout.com/eth/mainnet/blocks/' + field.height + '">' + field.height + '</a></td>' + '<td>' + dateTimeString + '</td>' + '<td>' + variance + '' + misurev + '</td></tr>');


        });
        $.each(result.luck, function (i, field) {
          var lucks = field.luck * 100;
          lucks = lucks.toFixed(2);
          var uncle = field.uncleRate * 100;
          uncle = uncle.toFixed(2);
          var orphan = field.orphanRate * 100;
          orphan = orphan.toFixed(2);
          var misure = '%';
          $("#luck").append('<tr><td>' + i + '<td>' + lucks + ' ' + misure + '</td>' + '<td>' + orphan + ' ' + misure + '</td>' + '<td>' + uncle + ' ' + misure + '</td></tr>');

        });
      });
    });
  });
});

$(document).ready(function () {
  setInterval(function () {
    $('tbody').empty();

    $.getJSON("https://eth.protonmine.com/api/blocks", function (result) {
      $.getJSON("https://eth.protonmine.com/api/stats", function (data) {
        $.getJSON('https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD', function (dataz) {

          $('#total').text(result.maturedTotal);
          $('#immature').text(result.immatureTotal);
          $('#candidate').text(result.candidatesTotal);
          $.each(result.matured, function (i, field) {

            var dateTimeString = moment.unix(field.timestamp).format("DD-MM-YYYY HH:mm:ss");
            var amount = field.reward / 1000000000000000000;
            amount = amount.toFixed(3);
            var misure = 'ETH';
            var usd = amount * dataz.USD;
            usd = usd.toFixed(1);
            var misureu = '$';
            var variance = field.shares / data.nodes[0].difficulty * 100;
            variance = variance.toFixed(1);
            var misurev = '%';
            $("#data").append('<tr><td><a rel="nofollow" target="_blank" href="https://blockscout.com/eth/mainnet/blocks/' + field.height + '">' + field.height + '</a></td>' + '<td>' + amount + ' ' + misure + ' ' + '/' + ' ' + usd + misureu + '</td>' + '<td>' + field.hash + '</td>' + '<td>' + dateTimeString + '</td>' + '<td>' + variance + '' + misurev + '</td></tr>');

          });
          $.each(result.immature, function (i, lul) {

            var dateTimeString = moment.unix(lul.timestamp).format("DD-MM-YYYY HH:mm:ss");
            var amount = lul.reward / 1000000000000000000;
            amount = amount.toFixed(3);
            var misure = misureeth;
            var usd = amount * dataz.USD;
            usd = usd.toFixed(1);
            var misureu = '$';
            var variance = lul.shares / data.nodes[0].difficulty * 100;
            variance = variance.toFixed(1);
            var misurev = '%';
            $("#immature").append('<tr><td><a rel="nofollow" target="_blank" href="https://blockscout.com/eth/mainnet/blocks/' + lul.height + '">' + lul.height + '</a></td>' + '<td>' + amount + ' ' + misure + ' ' + '/' + ' ' + usd + misureu + '</td>' + '<td>' + lul.hash + '</td>' + '<td>' + dateTimeString + '</td>' + '<td>' + variance + '' + misurev + '</td></tr>');

          });
          $.each(result.candidates, function (i, field) {

            var dateTimeString = moment.unix(field.timestamp).format("DD-MM-YYYY HH:mm:ss");
            var amount = field.reward / 1000000000000000000;
            amount = amount.toFixed(3);
            var misure = misureeth;
            var usd = amount * dataz.USD;
            usd = usd.toFixed(1);
            var misureu = '$';
            var variance = field.shares / data.nodes[0].difficulty * 100;
            variance = variance.toFixed(1);
            var misurev = '%';
            $("#new").append('<tr><td><a rel="nofollow" target="_blank" href="https://blockscout.com/eth/mainnet/blocks/' + field.height + '">' + field.height + '</a></td>' + '<td>' + dateTimeString + '</td>' + '<td>' + variance + '' + misurev + '</td></tr>');


          });
          $.each(result.luck, function (i, field) {
            var lucks = field.luck * 100;
            lucks = lucks.toFixed(2);
            var uncle = field.uncleRate * 100;
            uncle = uncle.toFixed(2);
            var orphan = field.orphanRate * 100;
            orphan = orphan.toFixed(2);
            var misure = '%';
            $("#luck").append('<tr><td>' + i + '<td>' + lucks + ' ' + misure + '</td>' + '<td>' + orphan + ' ' + misure + '</td>' + '<td>' + uncle + ' ' + misure + '</td></tr>');

          });
        });
      });
    });
  }, 120000);
});