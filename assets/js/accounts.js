
$(document).ready(function () {
  $.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    return results[1] || 0;
  }
  var url_param = $.urlParam('address');

  $.getJSON('http://clo.ccmpool.ro/api/accounts/' + url_param, function (data) {
    $.getJSON('https://eth.protonmine.com/api/stats', function (datas) {

      $('tbody').empty();


      $.each(data.rewards, function (i, field) {
        reward = field.reward / 1000000000;
        reward = reward.toFixed(2);
        var misurez = 'ETH';
        var variance = field.percent * 100;
        variance = variance.toFixed(1);
        var misure = '%';
        $("#rewards").append('<tr><td>' + field.blockheight + '<td>' + reward + ' ' + misurez + '</td><td>' + variance + ' ' + misure + '</td></tr>');
      });
      $.each(data.sumrewards, function (i, field) {
        reward = field.reward / 1000000000;
        reward = reward.toFixed(1);
        var misure = 'ETH';
        $("#sumreward").append('<tr><td>' + field.name + '<td>' + reward + ' ' + misure + '</td></tr>');
      });
      $.each(data.payments, function (i, field) {
        var dateTimeString = moment.unix(field.timestamp).format("DD-MM-YYYY HH:mm:ss");
        reward = field.amount / 1000000000;
        reward = reward.toFixed(1);
        var misure = 'ETH';
        $("#payments").append('<tr><td>' + dateTimeString + '<td>' + field.tx + '</td><td>' + reward + ' ' + misure + '</td></tr>');
      });

      $.each(data.workers, function (i, field) {

        var hr = field.hr;
        if (hr >= 1000000000) {
          var hash = field.hr / 1000000000;
          hash = hash.toFixed(2);
          var misure = 'Gh';
        } else {
          var hash = field.hr / 1000000;
          hash = hash.toFixed(2);
          var misure = 'Mh';
        }
        var hrz = field.hr2;
        if (hrz >= 1000000000) {
          var hashz = field.hr2 / 1000000000;
          hashz = hashz.toFixed(2);
          var misure = 'Gh';
        } else {
          var hashz = field.hr2 / 1000000;
          hashz = hashz.toFixed(2);
          var misure = 'Mh';
        }
        var difficulty = field.difficulty;
        if (difficulty >= 100000000) {
          var diff = field.difficulty / 1000000000;
          diff = diff.toFixed();
          var misurez = 'B';
        } else {
          var diff = field.difficulty / 100000000000;
          diff = diff.toFixed();
          var misurez = 'TH';
        }
        var day = moment.unix(field.lastBeat).fromNow();

        $("#worker").append('<tr><td>' + i + '<td>' + hash + ' ' + misure + '</td>' + '<td>' + hashz + ' ' + misure + '</td>' + '<td><span class="badge outline-badge-primary">' + diff + '' + misurez + ' </span></td>' + '<td>' + day + '</td></tr>');

      });
      //round variance
      var variance = data.roundShares / datas.stats.nShares * 100
      variance = variance.toFixed(1);
      var misure = '%';
      $('#variance').text(variance + '' + misure);
      //end round variance
      var lasttwenty = data.sumrewards[2].reward / 1000000000
      lasttwenty = lasttwenty.toFixed(2);
      var misure = 'Eth';
      $('#last').text(lasttwenty + ' ' + misure);

      var pending = data.stats.pending / 1000000000
      pending = pending.toFixed(2);
      var misure = 'Eth';
      $('#pending').text(pending + ' ' + misure);

      var immature = data.stats.immature / 1000000000
      immature = immature.toFixed(2);
      var misure = 'Eth';
      $('#immature').text(immature + ' ' + misure);

      var balance = data.stats.balance / 1000000000
      balance = balance.toFixed(2);
      var misure = 'Eth';
      $('#balance').text(balance + ' ' + misure);

      var paid = data.stats.paid / 1000000000
      paid = paid.toFixed(2);
      var misure = 'Eth';
      $('#paid').text(paid + ' ' + misure);

      var earnperday = 24 * 60 * 60 / 13 * 2 * data.hashrate / 10000000000;
      earnperday = earnperday.toFixed(2);
      var misure = 'Eth';
      $('#day').text(earnperday + ' ' + misure);

      //pool hashrate
      var hashrate = data.currentHashrate;
      if (hashrate >= 1000000000) {
        var hash = data.currentHashrate / 1000000000;
        hash = hash.toFixed(2);
        var misure = 'Gh';
      } else {
        var hash = data.currentHashrate / 1000000;
        hash = hash.toFixed(2);
        var misure = 'Mh';
      }
      $('#hashrate').text(hash + ' ' + misure);
      //end pool hashrate

      $('#miners').text(data.workersTotal);

      $('#found').text(data.stats.blocksFound);


      var day = moment.unix(data.stats.lastShare).fromNow();
      $('#share').text(day);
    });
  });


  var url_param = $.urlParam('address');
  setInterval(function () {

    $.getJSON('http://clo.ccmpool.ro/api/accounts/' + url_param, function (data) {
      $.getJSON('https://eth.protonmine.com/api/stats', function (datas) {
        $('tbody').empty();
        $.each(data.rewards, function (i, field) {
          reward = field.reward / 1000000000;
          reward = reward.toFixed(2);
          var misurez = 'ETH';
          var variance = field.percent * 100;
          variance = variance.toFixed(1);
          var misure = '%';
          $("#rewards").append('<tr><td>' + field.blockheight + '<td>' + reward + ' ' + misurez + '</td><td>' + variance + ' ' + misure + '</td></tr>');
        });
        $.each(data.sumrewards, function (i, field) {
          reward = field.reward / 1000000000;
          reward = reward.toFixed(1);
          var misure = 'ETH';
          $("#sumreward").append('<tr><td>' + field.name + '<td>' + reward + ' ' + misure + '</td></tr>');
        });
        $.each(data.payments, function (i, field) {
          var dateTimeString = moment.unix(field.timestamp).format("DD-MM-YYYY HH:mm:ss");
          reward = field.amount / 1000000000;
          reward = reward.toFixed(1);
          var misure = 'ETH';
          $("#payments").append('<tr><td>' + dateTimeString + '<td>' + field.tx + '</td><td>' + reward + ' ' + misure + '</td></tr>');
        });

        $.each(data.workers, function (i, field) {

          var hr = field.hr;
          if (hr >= 1000000000) {
            var hash = field.hr / 1000000000;
            hash = hash.toFixed(2);
            var misure = 'Gh';
          } else {
            var hash = field.hr / 1000000;
            hash = hash.toFixed(2);
            var misure = 'Mh';
          }
          var hrz = field.hr2;
          if (hrz >= 1000000000) {
            var hashz = field.hr2 / 1000000000;
            hashz = hashz.toFixed(2);
            var misure = 'Gh';
          } else {
            var hashz = field.hr2 / 1000000;
            hashz = hashz.toFixed(2);
            var misure = 'Mh';
          }
          var difficulty = field.difficulty;
          if (difficulty >= 100000000) {
            var diff = field.difficulty / 1000000000;
            diff = diff.toFixed();
            var misurez = 'B';
          } else {
            var diff = field.difficulty / 100000000000;
            diff = diff.toFixed();
            var misurez = 'TH';
          }
          var day = moment.unix(field.lastBeat).fromNow();

          $("#worker").append('<tr><td>' + i + '<td>' + hash + ' ' + misure + '</td>' + '<td>' + hashz + ' ' + misure + '</td>' + '<td><span class="badge outline-badge-primary">' + diff + '' + misurez + ' </span></td>' + '<td>' + day + '</td></tr>');

        });

        //round variance
        var variance = data.roundShares / datas.stats.nShares * 100
        variance = variance.toFixed(1);
        var misure = '%';
        $('#variance').text(variance + '' + misure);
        //end round variance
        var lasttwenty = data.sumrewards[2].reward / 1000000000
        lasttwenty = lasttwenty.toFixed(2);
        var misure = 'Eth';
        $('#last').text(lasttwenty + ' ' + misure);

        var pending = data.stats.pending / 1000000000
        pending = pending.toFixed(2);
        var misure = 'Eth';
        $('#pending').text(pending + ' ' + misure);

        var immature = data.stats.immature / 1000000000
        immature = immature.toFixed(2);
        var misure = 'Eth';
        $('#immature').text(immature + ' ' + misure);

        var balance = data.stats.balance / 1000000000
        balance = balance.toFixed(2);
        var misure = 'Eth';
        $('#balance').text(balance + ' ' + misure);

        var paid = data.stats.paid / 1000000000
        paid = paid.toFixed(2);
        var misure = 'Eth';
        $('#paid').text(paid + ' ' + misure);

        var earnperday = 24 * 60 * 60 / 13 * 2 * data.hashrate / 10000000000;
        earnperday = earnperday.toFixed(2);
        var misure = 'Eth';
        $('#day').text(earnperday + ' ' + misure);

        //pool hashrate
        var hashrate = data.currentHashrate;
        if (hashrate >= 1000000000) {
          var hash = data.currentHashrate / 1000000000;
          hash = hash.toFixed(2);
          var misure = 'Gh';
        } else {
          var hash = data.currentHashrate / 1000000;
          hash = hash.toFixed(2);
          var misure = 'Mh';
        }
        $('#hashrate').text(hash + ' ' + misure);
        //end pool hashrate

        $('#miners').text(data.workersTotal);

        $('#found').text(data.stats.blocksFound);


        var day = moment.unix(data.stats.lastShare).fromNow();
        $('#share').text(day);
      });
    });
  }, 3000);
});
$(document).ready(function () {
  $.getJSON('https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD', function (data) {
    var misure = '$';
    $('#price').text(data.USD + ' ' + misure);

  });
});
$(document).ready(function () {
  setInterval(function () {
    $.getJSON('https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD', function (data) {
      var misure = '$';
      $('#price').text(data.USD + ' ' + misure);

    });
  }, 120000);
});