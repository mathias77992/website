$(document).ready(function () {
  $.getJSON('https://eth.protonmine.com/api/stats', function (data) {

    //pool hashrate
    var hashrate = data.hashrate;
    if (hashrate >= 1000000000) {
      var hash = data.hashrate / 1000000000;
      hash = hash.toFixed(2);
      var misure = 'Gh';
    } else {
      var hash = data.hashrate / 1000000;
      hash = hash.toFixed(2);
      var misure = 'Mh';
    }
    $('#hashrate').text(hash + ' ' + misure);
    //end pool hashrate

    //total miners
    $('#miners').text(data.minersTotal);
    //end total miners

    //network hashrate
    var nethashrate = data.nodes[0].difficulty / 11.5;
    if (nethashrate >= 1000000000000) {
      var hash = data.nodes[0].difficulty / 13 / 1000000000000;
      hash = hash.toFixed(2);
      var misure = 'Th';
    }
    else {
      var hash = data.nodes[0].difficulty / 13 / 1000000000;
      hash = hash.toFixed(2);
      var misure = 'Gh';
    }
    $('#nethashrate').text(hash + ' ' + misure);
    //end network hashrate

    //Network difficulty
    var difficulty = data.nodes[0].difficulty;
    if (difficulty >= 1000000000000000) {
      var diff = data.nodes[0].difficulty / 1000000000000000;
      diff = diff.toFixed(2);
      var misure = 'P';
    } else {
      var diff = data.nodes[0].difficulty / 1000000000000;
      diff = diff.toFixed();
      var misure = 'TH';
    }
    $('#diff').text(diff + ' ' + misure);
    //end network difficulty
    //round variance
    var variance = data.stats.roundShares / data.nodes[0].difficulty * 100
    variance = variance.toFixed(1);
    var misure = '%';
    $('#variance').text(variance + '' + misure);
    //end round variance

    //block height 
    $('#height').text(data.nodes[0].height);
    //end block height


    //block found
    var day = moment.unix(data.stats.lastBlockFound).fromNow();
    var blockExists = (day === 'a few seconds ago') ? "Never" : day;
    $('#found').text(blockExists);
    //end block found

  });
});
$(document).ready(function () {
  setInterval(function () {
    $.getJSON('https://eth.protonmine.com/api/stats', function (data) {

      //pool hashrate
      var hashrate = data.hashrate;
      if (hashrate >= 1000000000) {
        var hash = data.hashrate / 1000000000;
        hash = hash.toFixed(2);
        var misure = 'Gb';
      } else {
        var hash = data.hashrate / 1000000;
        hash = hash.toFixed(2);
        var misure = 'Mh';
      }
      $('#hashrate').text(hash + ' ' + misure);
      //end pool hashrate

      //total miners
      $('#miners').text(data.minersTotal);
      //end total miners

      //network hashrate
      var nethashrate = data.nodes[0].difficulty / 11.5;
      if (nethashrate >= 1000000000000) {
        var hash = data.nodes[0].difficulty / 13 / 1000000000000;
        hash = hash.toFixed(2);
        var misure = 'Th';
      }
      else {
        var hash = data.nodes[0].difficulty / 13 / 1000000000;
        hash = hash.toFixed(2);
        var misure = 'Gh';
      }
      $('#nethashrate').text(hash + ' ' + misure);
      //end network hashrate

      //Network difficulty
      var difficulty = data.nodes[0].difficulty;
      if (difficulty >= 1000000000000000) {
        var diff = data.nodes[0].difficulty / 1000000000000000;
        diff = diff.toFixed(2);
        var misure = 'P';
      } else {
        var diff = data.nodes[0].difficulty / 1000000000000;
        diff = diff.toFixed();
        var misure = 'TH';
      }
      $('#diff').text(diff + ' ' + misure);
      //end network difficulty
      //round variance
      var variance = data.stats.roundShares / data.nodes[0].difficulty * 100
      variance = variance.toFixed(1);
      var misure = '%';
      $('#variance').text(variance + '' + misure);
      //end round variance

      //block height 
      $('#height').text(data.nodes[0].height);
      //end block height


      //block found
      var day = moment.unix(data.stats.lastBlockFound).fromNow();
      var blockExists = (day === 'a few seconds ago') ? "Never" : day;
      $('#found').text(blockExists);
      //end block found

    });
  }, 5000);
});
$(document).ready(function () {
  $.getJSON('https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD', function (data) {
    var misure = '$';
    $('#price').text(data.USD + ' ' + misure);

  });
});
$(document).ready(function () {
  setInterval(function () {
    $.getJSON('https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD', function (data) {
      var misure = '$';
      $('#price').text(data.USD + ' ' + misure);

    });
  }, 120000);
});

var form = document.querySelector("form");
console.log(form.elements.name);
